import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DassuGatewayTestModule } from '../../../../test.module';
import { CompositFileComponent } from 'app/entities/DassuCore/composit-file/composit-file.component';
import { CompositFileService } from 'app/entities/DassuCore/composit-file/composit-file.service';
import { CompositFile } from 'app/shared/model/DassuCore/composit-file.model';

describe('Component Tests', () => {
  describe('CompositFile Management Component', () => {
    let comp: CompositFileComponent;
    let fixture: ComponentFixture<CompositFileComponent>;
    let service: CompositFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [CompositFileComponent],
      })
        .overrideTemplate(CompositFileComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CompositFileComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CompositFileService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CompositFile(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.compositFiles && comp.compositFiles[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
