import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { CompositFileDetailComponent } from 'app/entities/DassuCore/composit-file/composit-file-detail.component';
import { CompositFile } from 'app/shared/model/DassuCore/composit-file.model';

describe('Component Tests', () => {
  describe('CompositFile Management Detail Component', () => {
    let comp: CompositFileDetailComponent;
    let fixture: ComponentFixture<CompositFileDetailComponent>;
    const route = ({ data: of({ compositFile: new CompositFile(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [CompositFileDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CompositFileDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CompositFileDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load compositFile on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.compositFile).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
