import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { CompositFileUpdateComponent } from 'app/entities/DassuCore/composit-file/composit-file-update.component';
import { CompositFileService } from 'app/entities/DassuCore/composit-file/composit-file.service';
import { CompositFile } from 'app/shared/model/DassuCore/composit-file.model';

describe('Component Tests', () => {
  describe('CompositFile Management Update Component', () => {
    let comp: CompositFileUpdateComponent;
    let fixture: ComponentFixture<CompositFileUpdateComponent>;
    let service: CompositFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [CompositFileUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CompositFileUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CompositFileUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CompositFileService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CompositFile(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CompositFile();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
