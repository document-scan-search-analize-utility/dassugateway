import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryFieldValueUpdateComponent } from 'app/entities/DassuCore/file-category-field-value/file-category-field-value-update.component';
import { FileCategoryFieldValueService } from 'app/entities/DassuCore/file-category-field-value/file-category-field-value.service';
import { FileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';

describe('Component Tests', () => {
  describe('FileCategoryFieldValue Management Update Component', () => {
    let comp: FileCategoryFieldValueUpdateComponent;
    let fixture: ComponentFixture<FileCategoryFieldValueUpdateComponent>;
    let service: FileCategoryFieldValueService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryFieldValueUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FileCategoryFieldValueUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FileCategoryFieldValueUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FileCategoryFieldValueService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FileCategoryFieldValue(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FileCategoryFieldValue();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
