import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryFieldValueDetailComponent } from 'app/entities/DassuCore/file-category-field-value/file-category-field-value-detail.component';
import { FileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';

describe('Component Tests', () => {
  describe('FileCategoryFieldValue Management Detail Component', () => {
    let comp: FileCategoryFieldValueDetailComponent;
    let fixture: ComponentFixture<FileCategoryFieldValueDetailComponent>;
    const route = ({ data: of({ fileCategoryFieldValue: new FileCategoryFieldValue(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryFieldValueDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FileCategoryFieldValueDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FileCategoryFieldValueDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fileCategoryFieldValue on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fileCategoryFieldValue).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
