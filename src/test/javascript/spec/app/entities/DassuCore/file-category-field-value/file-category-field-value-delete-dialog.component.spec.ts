import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { DassuGatewayTestModule } from '../../../../test.module';
import { MockEventManager } from '../../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../../helpers/mock-active-modal.service';
import { FileCategoryFieldValueDeleteDialogComponent } from 'app/entities/DassuCore/file-category-field-value/file-category-field-value-delete-dialog.component';
import { FileCategoryFieldValueService } from 'app/entities/DassuCore/file-category-field-value/file-category-field-value.service';

describe('Component Tests', () => {
  describe('FileCategoryFieldValue Management Delete Component', () => {
    let comp: FileCategoryFieldValueDeleteDialogComponent;
    let fixture: ComponentFixture<FileCategoryFieldValueDeleteDialogComponent>;
    let service: FileCategoryFieldValueService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryFieldValueDeleteDialogComponent],
      })
        .overrideTemplate(FileCategoryFieldValueDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FileCategoryFieldValueDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FileCategoryFieldValueService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
