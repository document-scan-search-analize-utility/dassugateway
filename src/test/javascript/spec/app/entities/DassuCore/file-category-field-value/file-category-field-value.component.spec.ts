import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryFieldValueComponent } from 'app/entities/DassuCore/file-category-field-value/file-category-field-value.component';
import { FileCategoryFieldValueService } from 'app/entities/DassuCore/file-category-field-value/file-category-field-value.service';
import { FileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';

describe('Component Tests', () => {
  describe('FileCategoryFieldValue Management Component', () => {
    let comp: FileCategoryFieldValueComponent;
    let fixture: ComponentFixture<FileCategoryFieldValueComponent>;
    let service: FileCategoryFieldValueService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryFieldValueComponent],
      })
        .overrideTemplate(FileCategoryFieldValueComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FileCategoryFieldValueComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FileCategoryFieldValueService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FileCategoryFieldValue(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fileCategoryFieldValues && comp.fileCategoryFieldValues[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
