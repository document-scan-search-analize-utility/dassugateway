import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { BinaryFileDetailComponent } from 'app/entities/DassuCore/binary-file/binary-file-detail.component';
import { BinaryFile } from 'app/shared/model/DassuCore/binary-file.model';

describe('Component Tests', () => {
  describe('BinaryFile Management Detail Component', () => {
    let comp: BinaryFileDetailComponent;
    let fixture: ComponentFixture<BinaryFileDetailComponent>;
    const route = ({ data: of({ binaryFile: new BinaryFile(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [BinaryFileDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BinaryFileDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BinaryFileDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load binaryFile on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.binaryFile).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
