import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DassuGatewayTestModule } from '../../../../test.module';
import { BinaryFileComponent } from 'app/entities/DassuCore/binary-file/binary-file.component';
import { BinaryFileService } from 'app/entities/DassuCore/binary-file/binary-file.service';
import { BinaryFile } from 'app/shared/model/DassuCore/binary-file.model';

describe('Component Tests', () => {
  describe('BinaryFile Management Component', () => {
    let comp: BinaryFileComponent;
    let fixture: ComponentFixture<BinaryFileComponent>;
    let service: BinaryFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [BinaryFileComponent],
      })
        .overrideTemplate(BinaryFileComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BinaryFileComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BinaryFileService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BinaryFile(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.binaryFiles && comp.binaryFiles[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
