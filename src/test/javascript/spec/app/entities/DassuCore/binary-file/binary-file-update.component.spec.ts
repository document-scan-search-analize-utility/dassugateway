import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { BinaryFileUpdateComponent } from 'app/entities/DassuCore/binary-file/binary-file-update.component';
import { BinaryFileService } from 'app/entities/DassuCore/binary-file/binary-file.service';
import { BinaryFile } from 'app/shared/model/DassuCore/binary-file.model';

describe('Component Tests', () => {
  describe('BinaryFile Management Update Component', () => {
    let comp: BinaryFileUpdateComponent;
    let fixture: ComponentFixture<BinaryFileUpdateComponent>;
    let service: BinaryFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [BinaryFileUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BinaryFileUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BinaryFileUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BinaryFileService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BinaryFile(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BinaryFile();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
