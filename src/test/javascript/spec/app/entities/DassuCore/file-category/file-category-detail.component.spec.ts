import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryDetailComponent } from 'app/entities/DassuCore/file-category/file-category-detail.component';
import { FileCategory } from 'app/shared/model/DassuCore/file-category.model';

describe('Component Tests', () => {
  describe('FileCategory Management Detail Component', () => {
    let comp: FileCategoryDetailComponent;
    let fixture: ComponentFixture<FileCategoryDetailComponent>;
    const route = ({ data: of({ fileCategory: new FileCategory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FileCategoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FileCategoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fileCategory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fileCategory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
