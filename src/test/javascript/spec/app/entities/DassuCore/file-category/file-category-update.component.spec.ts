import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryUpdateComponent } from 'app/entities/DassuCore/file-category/file-category-update.component';
import { FileCategoryService } from 'app/entities/DassuCore/file-category/file-category.service';
import { FileCategory } from 'app/shared/model/DassuCore/file-category.model';

describe('Component Tests', () => {
  describe('FileCategory Management Update Component', () => {
    let comp: FileCategoryUpdateComponent;
    let fixture: ComponentFixture<FileCategoryUpdateComponent>;
    let service: FileCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FileCategoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FileCategoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FileCategoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FileCategory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FileCategory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
