import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryFieldDetailComponent } from 'app/entities/DassuCore/file-category-field/file-category-field-detail.component';
import { FileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';

describe('Component Tests', () => {
  describe('FileCategoryField Management Detail Component', () => {
    let comp: FileCategoryFieldDetailComponent;
    let fixture: ComponentFixture<FileCategoryFieldDetailComponent>;
    const route = ({ data: of({ fileCategoryField: new FileCategoryField(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryFieldDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FileCategoryFieldDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FileCategoryFieldDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fileCategoryField on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fileCategoryField).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
