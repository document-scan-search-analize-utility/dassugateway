import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryFieldUpdateComponent } from 'app/entities/DassuCore/file-category-field/file-category-field-update.component';
import { FileCategoryFieldService } from 'app/entities/DassuCore/file-category-field/file-category-field.service';
import { FileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';

describe('Component Tests', () => {
  describe('FileCategoryField Management Update Component', () => {
    let comp: FileCategoryFieldUpdateComponent;
    let fixture: ComponentFixture<FileCategoryFieldUpdateComponent>;
    let service: FileCategoryFieldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryFieldUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FileCategoryFieldUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FileCategoryFieldUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FileCategoryFieldService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FileCategoryField(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FileCategoryField();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
