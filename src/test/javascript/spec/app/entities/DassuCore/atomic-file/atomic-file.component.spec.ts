import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DassuGatewayTestModule } from '../../../../test.module';
import { AtomicFileComponent } from 'app/entities/DassuCore/atomic-file/atomic-file.component';
import { AtomicFileService } from 'app/entities/DassuCore/atomic-file/atomic-file.service';
import { AtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';

describe('Component Tests', () => {
  describe('AtomicFile Management Component', () => {
    let comp: AtomicFileComponent;
    let fixture: ComponentFixture<AtomicFileComponent>;
    let service: AtomicFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [AtomicFileComponent],
      })
        .overrideTemplate(AtomicFileComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AtomicFileComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AtomicFileService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AtomicFile(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.atomicFiles && comp.atomicFiles[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
