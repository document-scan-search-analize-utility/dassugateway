import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { AtomicFileUpdateComponent } from 'app/entities/DassuCore/atomic-file/atomic-file-update.component';
import { AtomicFileService } from 'app/entities/DassuCore/atomic-file/atomic-file.service';
import { AtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';

describe('Component Tests', () => {
  describe('AtomicFile Management Update Component', () => {
    let comp: AtomicFileUpdateComponent;
    let fixture: ComponentFixture<AtomicFileUpdateComponent>;
    let service: AtomicFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [AtomicFileUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AtomicFileUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AtomicFileUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AtomicFileService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AtomicFile(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AtomicFile();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
