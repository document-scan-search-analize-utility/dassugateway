import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DassuGatewayTestModule } from '../../../../test.module';
import { MetricTypeComponent } from 'app/entities/DassuCore/metric-type/metric-type.component';
import { MetricTypeService } from 'app/entities/DassuCore/metric-type/metric-type.service';
import { MetricType } from 'app/shared/model/DassuCore/metric-type.model';

describe('Component Tests', () => {
  describe('MetricType Management Component', () => {
    let comp: MetricTypeComponent;
    let fixture: ComponentFixture<MetricTypeComponent>;
    let service: MetricTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [MetricTypeComponent],
      })
        .overrideTemplate(MetricTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MetricTypeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MetricTypeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MetricType(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.metricTypes && comp.metricTypes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
