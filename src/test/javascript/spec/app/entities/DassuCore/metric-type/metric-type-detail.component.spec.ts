import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { MetricTypeDetailComponent } from 'app/entities/DassuCore/metric-type/metric-type-detail.component';
import { MetricType } from 'app/shared/model/DassuCore/metric-type.model';

describe('Component Tests', () => {
  describe('MetricType Management Detail Component', () => {
    let comp: MetricTypeDetailComponent;
    let fixture: ComponentFixture<MetricTypeDetailComponent>;
    const route = ({ data: of({ metricType: new MetricType(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [MetricTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MetricTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MetricTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load metricType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.metricType).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
