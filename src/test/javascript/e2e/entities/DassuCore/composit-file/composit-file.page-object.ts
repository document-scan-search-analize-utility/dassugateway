import { element, by, ElementFinder } from 'protractor';

export class CompositFileComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-composit-file div table .btn-danger'));
  title = element.all(by.css('jhi-composit-file div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CompositFileUpdatePage {
  pageTitle = element(by.id('jhi-composit-file-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  statusSelect = element(by.id('field_status'));

  binaryFileSelect = element(by.id('field_binaryFile'));
  fileCategorySelect = element(by.id('field_fileCategory'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setStatusSelect(status: string): Promise<void> {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect(): Promise<string> {
    return await this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption(): Promise<void> {
    await this.statusSelect.all(by.tagName('option')).last().click();
  }

  async binaryFileSelectLastOption(): Promise<void> {
    await this.binaryFileSelect.all(by.tagName('option')).last().click();
  }

  async binaryFileSelectOption(option: string): Promise<void> {
    await this.binaryFileSelect.sendKeys(option);
  }

  getBinaryFileSelect(): ElementFinder {
    return this.binaryFileSelect;
  }

  async getBinaryFileSelectedOption(): Promise<string> {
    return await this.binaryFileSelect.element(by.css('option:checked')).getText();
  }

  async fileCategorySelectLastOption(): Promise<void> {
    await this.fileCategorySelect.all(by.tagName('option')).last().click();
  }

  async fileCategorySelectOption(option: string): Promise<void> {
    await this.fileCategorySelect.sendKeys(option);
  }

  getFileCategorySelect(): ElementFinder {
    return this.fileCategorySelect;
  }

  async getFileCategorySelectedOption(): Promise<string> {
    return await this.fileCategorySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CompositFileDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-compositFile-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-compositFile'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
