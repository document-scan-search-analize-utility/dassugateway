import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { MetricTypeComponentsPage, MetricTypeDeleteDialog, MetricTypeUpdatePage } from './metric-type.page-object';

const expect = chai.expect;

describe('MetricType e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let metricTypeComponentsPage: MetricTypeComponentsPage;
  let metricTypeUpdatePage: MetricTypeUpdatePage;
  let metricTypeDeleteDialog: MetricTypeDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load MetricTypes', async () => {
    await navBarPage.goToEntity('metric-type');
    metricTypeComponentsPage = new MetricTypeComponentsPage();
    await browser.wait(ec.visibilityOf(metricTypeComponentsPage.title), 5000);
    expect(await metricTypeComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreMetricType.home.title');
    await browser.wait(ec.or(ec.visibilityOf(metricTypeComponentsPage.entities), ec.visibilityOf(metricTypeComponentsPage.noResult)), 1000);
  });

  it('should load create MetricType page', async () => {
    await metricTypeComponentsPage.clickOnCreateButton();
    metricTypeUpdatePage = new MetricTypeUpdatePage();
    expect(await metricTypeUpdatePage.getPageTitle()).to.eq('dassuGatewayApp.dassuCoreMetricType.home.createOrEditLabel');
    await metricTypeUpdatePage.cancel();
  });

  it('should create and save MetricTypes', async () => {
    const nbButtonsBeforeCreate = await metricTypeComponentsPage.countDeleteButtons();

    await metricTypeComponentsPage.clickOnCreateButton();

    await promise.all([metricTypeUpdatePage.setNameInput('name')]);

    expect(await metricTypeUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');

    await metricTypeUpdatePage.save();
    expect(await metricTypeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await metricTypeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last MetricType', async () => {
    const nbButtonsBeforeDelete = await metricTypeComponentsPage.countDeleteButtons();
    await metricTypeComponentsPage.clickOnLastDeleteButton();

    metricTypeDeleteDialog = new MetricTypeDeleteDialog();
    expect(await metricTypeDeleteDialog.getDialogTitle()).to.eq('dassuGatewayApp.dassuCoreMetricType.delete.question');
    await metricTypeDeleteDialog.clickOnConfirmButton();

    expect(await metricTypeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
