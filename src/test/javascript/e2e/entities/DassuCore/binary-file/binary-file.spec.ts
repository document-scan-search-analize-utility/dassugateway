import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { BinaryFileComponentsPage, BinaryFileDeleteDialog, BinaryFileUpdatePage } from './binary-file.page-object';

const expect = chai.expect;

describe('BinaryFile e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let binaryFileComponentsPage: BinaryFileComponentsPage;
  let binaryFileUpdatePage: BinaryFileUpdatePage;
  let binaryFileDeleteDialog: BinaryFileDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BinaryFiles', async () => {
    await navBarPage.goToEntity('binary-file');
    binaryFileComponentsPage = new BinaryFileComponentsPage();
    await browser.wait(ec.visibilityOf(binaryFileComponentsPage.title), 5000);
    expect(await binaryFileComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreBinaryFile.home.title');
    await browser.wait(ec.or(ec.visibilityOf(binaryFileComponentsPage.entities), ec.visibilityOf(binaryFileComponentsPage.noResult)), 1000);
  });

  it('should load create BinaryFile page', async () => {
    await binaryFileComponentsPage.clickOnCreateButton();
    binaryFileUpdatePage = new BinaryFileUpdatePage();
    expect(await binaryFileUpdatePage.getPageTitle()).to.eq('dassuGatewayApp.dassuCoreBinaryFile.home.createOrEditLabel');
    await binaryFileUpdatePage.cancel();
  });

  it('should create and save BinaryFiles', async () => {
    const nbButtonsBeforeCreate = await binaryFileComponentsPage.countDeleteButtons();

    await binaryFileComponentsPage.clickOnCreateButton();

    await promise.all([
      binaryFileUpdatePage.setNameInput('name'),
      binaryFileUpdatePage.setVersionInput('5'),
      binaryFileUpdatePage.setStorageFilePathInput('storageFilePath'),
      binaryFileUpdatePage.setMd5Input('md5'),
      binaryFileUpdatePage.storageTypeSelectLastOption(),
    ]);

    expect(await binaryFileUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await binaryFileUpdatePage.getVersionInput()).to.eq('5', 'Expected version value to be equals to 5');
    expect(await binaryFileUpdatePage.getStorageFilePathInput()).to.eq(
      'storageFilePath',
      'Expected StorageFilePath value to be equals to storageFilePath'
    );
    expect(await binaryFileUpdatePage.getMd5Input()).to.eq('md5', 'Expected Md5 value to be equals to md5');

    await binaryFileUpdatePage.save();
    expect(await binaryFileUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await binaryFileComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last BinaryFile', async () => {
    const nbButtonsBeforeDelete = await binaryFileComponentsPage.countDeleteButtons();
    await binaryFileComponentsPage.clickOnLastDeleteButton();

    binaryFileDeleteDialog = new BinaryFileDeleteDialog();
    expect(await binaryFileDeleteDialog.getDialogTitle()).to.eq('dassuGatewayApp.dassuCoreBinaryFile.delete.question');
    await binaryFileDeleteDialog.clickOnConfirmButton();

    expect(await binaryFileComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
