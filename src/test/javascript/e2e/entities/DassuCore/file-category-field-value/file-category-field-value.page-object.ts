import { element, by, ElementFinder } from 'protractor';

export class FileCategoryFieldValueComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-file-category-field-value div table .btn-danger'));
  title = element.all(by.css('jhi-file-category-field-value div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FileCategoryFieldValueUpdatePage {
  pageTitle = element(by.id('jhi-file-category-field-value-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  valueInput = element(by.id('field_value'));

  compositFileSelect = element(by.id('field_compositFile'));
  fileCategoryFieldSelect = element(by.id('field_fileCategoryField'));
  metricSelect = element(by.id('field_metric'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setValueInput(value: string): Promise<void> {
    await this.valueInput.sendKeys(value);
  }

  async getValueInput(): Promise<string> {
    return await this.valueInput.getAttribute('value');
  }

  async compositFileSelectLastOption(): Promise<void> {
    await this.compositFileSelect.all(by.tagName('option')).last().click();
  }

  async compositFileSelectOption(option: string): Promise<void> {
    await this.compositFileSelect.sendKeys(option);
  }

  getCompositFileSelect(): ElementFinder {
    return this.compositFileSelect;
  }

  async getCompositFileSelectedOption(): Promise<string> {
    return await this.compositFileSelect.element(by.css('option:checked')).getText();
  }

  async fileCategoryFieldSelectLastOption(): Promise<void> {
    await this.fileCategoryFieldSelect.all(by.tagName('option')).last().click();
  }

  async fileCategoryFieldSelectOption(option: string): Promise<void> {
    await this.fileCategoryFieldSelect.sendKeys(option);
  }

  getFileCategoryFieldSelect(): ElementFinder {
    return this.fileCategoryFieldSelect;
  }

  async getFileCategoryFieldSelectedOption(): Promise<string> {
    return await this.fileCategoryFieldSelect.element(by.css('option:checked')).getText();
  }

  async metricSelectLastOption(): Promise<void> {
    await this.metricSelect.all(by.tagName('option')).last().click();
  }

  async metricSelectOption(option: string): Promise<void> {
    await this.metricSelect.sendKeys(option);
  }

  getMetricSelect(): ElementFinder {
    return this.metricSelect;
  }

  async getMetricSelectedOption(): Promise<string> {
    return await this.metricSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FileCategoryFieldValueDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-fileCategoryFieldValue-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-fileCategoryFieldValue'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
