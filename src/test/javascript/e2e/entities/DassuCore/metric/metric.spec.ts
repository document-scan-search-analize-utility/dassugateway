import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { MetricComponentsPage, MetricDeleteDialog, MetricUpdatePage } from './metric.page-object';

const expect = chai.expect;

describe('Metric e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let metricComponentsPage: MetricComponentsPage;
  let metricUpdatePage: MetricUpdatePage;
  let metricDeleteDialog: MetricDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Metrics', async () => {
    await navBarPage.goToEntity('metric');
    metricComponentsPage = new MetricComponentsPage();
    await browser.wait(ec.visibilityOf(metricComponentsPage.title), 5000);
    expect(await metricComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreMetric.home.title');
    await browser.wait(ec.or(ec.visibilityOf(metricComponentsPage.entities), ec.visibilityOf(metricComponentsPage.noResult)), 1000);
  });

  it('should load create Metric page', async () => {
    await metricComponentsPage.clickOnCreateButton();
    metricUpdatePage = new MetricUpdatePage();
    expect(await metricUpdatePage.getPageTitle()).to.eq('dassuGatewayApp.dassuCoreMetric.home.createOrEditLabel');
    await metricUpdatePage.cancel();
  });

  it('should create and save Metrics', async () => {
    const nbButtonsBeforeCreate = await metricComponentsPage.countDeleteButtons();

    await metricComponentsPage.clickOnCreateButton();

    await promise.all([
      metricUpdatePage.setKeyInput('key'),
      metricUpdatePage.setValueInput('5'),
      metricUpdatePage.metricTypeSelectLastOption(),
    ]);

    expect(await metricUpdatePage.getKeyInput()).to.eq('key', 'Expected Key value to be equals to key');
    expect(await metricUpdatePage.getValueInput()).to.eq('5', 'Expected value value to be equals to 5');

    await metricUpdatePage.save();
    expect(await metricUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await metricComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Metric', async () => {
    const nbButtonsBeforeDelete = await metricComponentsPage.countDeleteButtons();
    await metricComponentsPage.clickOnLastDeleteButton();

    metricDeleteDialog = new MetricDeleteDialog();
    expect(await metricDeleteDialog.getDialogTitle()).to.eq('dassuGatewayApp.dassuCoreMetric.delete.question');
    await metricDeleteDialog.clickOnConfirmButton();

    expect(await metricComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
