import { element, by, ElementFinder } from 'protractor';

export class MetricComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-metric div table .btn-danger'));
  title = element.all(by.css('jhi-metric div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MetricUpdatePage {
  pageTitle = element(by.id('jhi-metric-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  keyInput = element(by.id('field_key'));
  valueInput = element(by.id('field_value'));

  metricTypeSelect = element(by.id('field_metricType'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setKeyInput(key: string): Promise<void> {
    await this.keyInput.sendKeys(key);
  }

  async getKeyInput(): Promise<string> {
    return await this.keyInput.getAttribute('value');
  }

  async setValueInput(value: string): Promise<void> {
    await this.valueInput.sendKeys(value);
  }

  async getValueInput(): Promise<string> {
    return await this.valueInput.getAttribute('value');
  }

  async metricTypeSelectLastOption(): Promise<void> {
    await this.metricTypeSelect.all(by.tagName('option')).last().click();
  }

  async metricTypeSelectOption(option: string): Promise<void> {
    await this.metricTypeSelect.sendKeys(option);
  }

  getMetricTypeSelect(): ElementFinder {
    return this.metricTypeSelect;
  }

  async getMetricTypeSelectedOption(): Promise<string> {
    return await this.metricTypeSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MetricDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-metric-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-metric'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
