import { element, by, ElementFinder } from 'protractor';

export class AtomicFileComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-atomic-file div table .btn-danger'));
  title = element.all(by.css('jhi-atomic-file div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AtomicFileUpdatePage {
  pageTitle = element(by.id('jhi-atomic-file-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  annotatedDataInput = element(by.id('field_annotatedData'));
  annotatedTextInput = element(by.id('field_annotatedText'));

  binaryFileSelect = element(by.id('field_binaryFile'));
  parentSelect = element(by.id('field_parent'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setAnnotatedDataInput(annotatedData: string): Promise<void> {
    await this.annotatedDataInput.sendKeys(annotatedData);
  }

  async getAnnotatedDataInput(): Promise<string> {
    return await this.annotatedDataInput.getAttribute('value');
  }

  async setAnnotatedTextInput(annotatedText: string): Promise<void> {
    await this.annotatedTextInput.sendKeys(annotatedText);
  }

  async getAnnotatedTextInput(): Promise<string> {
    return await this.annotatedTextInput.getAttribute('value');
  }

  async binaryFileSelectLastOption(): Promise<void> {
    await this.binaryFileSelect.all(by.tagName('option')).last().click();
  }

  async binaryFileSelectOption(option: string): Promise<void> {
    await this.binaryFileSelect.sendKeys(option);
  }

  getBinaryFileSelect(): ElementFinder {
    return this.binaryFileSelect;
  }

  async getBinaryFileSelectedOption(): Promise<string> {
    return await this.binaryFileSelect.element(by.css('option:checked')).getText();
  }

  async parentSelectLastOption(): Promise<void> {
    await this.parentSelect.all(by.tagName('option')).last().click();
  }

  async parentSelectOption(option: string): Promise<void> {
    await this.parentSelect.sendKeys(option);
  }

  getParentSelect(): ElementFinder {
    return this.parentSelect;
  }

  async getParentSelectedOption(): Promise<string> {
    return await this.parentSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AtomicFileDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-atomicFile-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-atomicFile'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
