import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { AtomicFileComponentsPage, AtomicFileDeleteDialog, AtomicFileUpdatePage } from './atomic-file.page-object';

const expect = chai.expect;

describe('AtomicFile e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let atomicFileComponentsPage: AtomicFileComponentsPage;
  let atomicFileUpdatePage: AtomicFileUpdatePage;
  let atomicFileDeleteDialog: AtomicFileDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AtomicFiles', async () => {
    await navBarPage.goToEntity('atomic-file');
    atomicFileComponentsPage = new AtomicFileComponentsPage();
    await browser.wait(ec.visibilityOf(atomicFileComponentsPage.title), 5000);
    expect(await atomicFileComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreAtomicFile.home.title');
    await browser.wait(ec.or(ec.visibilityOf(atomicFileComponentsPage.entities), ec.visibilityOf(atomicFileComponentsPage.noResult)), 1000);
  });

  it('should load create AtomicFile page', async () => {
    await atomicFileComponentsPage.clickOnCreateButton();
    atomicFileUpdatePage = new AtomicFileUpdatePage();
    expect(await atomicFileUpdatePage.getPageTitle()).to.eq('dassuGatewayApp.dassuCoreAtomicFile.home.createOrEditLabel');
    await atomicFileUpdatePage.cancel();
  });

  it('should create and save AtomicFiles', async () => {
    const nbButtonsBeforeCreate = await atomicFileComponentsPage.countDeleteButtons();

    await atomicFileComponentsPage.clickOnCreateButton();

    await promise.all([
      atomicFileUpdatePage.setAnnotatedDataInput('annotatedData'),
      atomicFileUpdatePage.setAnnotatedTextInput('annotatedText'),
      atomicFileUpdatePage.binaryFileSelectLastOption(),
      atomicFileUpdatePage.parentSelectLastOption(),
    ]);

    expect(await atomicFileUpdatePage.getAnnotatedDataInput()).to.eq(
      'annotatedData',
      'Expected AnnotatedData value to be equals to annotatedData'
    );
    expect(await atomicFileUpdatePage.getAnnotatedTextInput()).to.eq(
      'annotatedText',
      'Expected AnnotatedText value to be equals to annotatedText'
    );

    await atomicFileUpdatePage.save();
    expect(await atomicFileUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await atomicFileComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last AtomicFile', async () => {
    const nbButtonsBeforeDelete = await atomicFileComponentsPage.countDeleteButtons();
    await atomicFileComponentsPage.clickOnLastDeleteButton();

    atomicFileDeleteDialog = new AtomicFileDeleteDialog();
    expect(await atomicFileDeleteDialog.getDialogTitle()).to.eq('dassuGatewayApp.dassuCoreAtomicFile.delete.question');
    await atomicFileDeleteDialog.clickOnConfirmButton();

    expect(await atomicFileComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
