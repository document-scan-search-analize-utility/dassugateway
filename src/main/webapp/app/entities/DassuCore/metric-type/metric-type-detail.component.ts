import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMetricType } from 'app/shared/model/DassuCore/metric-type.model';

@Component({
  selector: 'jhi-metric-type-detail',
  templateUrl: './metric-type-detail.component.html',
})
export class MetricTypeDetailComponent implements OnInit {
  metricType: IMetricType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ metricType }) => (this.metricType = metricType));
  }

  previousState(): void {
    window.history.back();
  }
}
