import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMetricType } from 'app/shared/model/DassuCore/metric-type.model';

type EntityResponseType = HttpResponse<IMetricType>;
type EntityArrayResponseType = HttpResponse<IMetricType[]>;

@Injectable({ providedIn: 'root' })
export class MetricTypeService {
  public resourceUrl = SERVER_API_URL + 'services/dassucore/api/metric-types';

  constructor(protected http: HttpClient) {}

  create(metricType: IMetricType): Observable<EntityResponseType> {
    return this.http.post<IMetricType>(this.resourceUrl, metricType, { observe: 'response' });
  }

  update(metricType: IMetricType): Observable<EntityResponseType> {
    return this.http.put<IMetricType>(this.resourceUrl, metricType, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMetricType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMetricType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
