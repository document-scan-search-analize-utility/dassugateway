import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMetricType } from 'app/shared/model/DassuCore/metric-type.model';
import { MetricTypeService } from './metric-type.service';

@Component({
  templateUrl: './metric-type-delete-dialog.component.html',
})
export class MetricTypeDeleteDialogComponent {
  metricType?: IMetricType;

  constructor(
    protected metricTypeService: MetricTypeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.metricTypeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('metricTypeListModification');
      this.activeModal.close();
    });
  }
}
