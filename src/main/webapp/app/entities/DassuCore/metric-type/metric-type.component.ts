import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMetricType } from 'app/shared/model/DassuCore/metric-type.model';
import { MetricTypeService } from './metric-type.service';
import { MetricTypeDeleteDialogComponent } from './metric-type-delete-dialog.component';

@Component({
  selector: 'jhi-metric-type',
  templateUrl: './metric-type.component.html',
})
export class MetricTypeComponent implements OnInit, OnDestroy {
  metricTypes?: IMetricType[];
  eventSubscriber?: Subscription;

  constructor(protected metricTypeService: MetricTypeService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.metricTypeService.query().subscribe((res: HttpResponse<IMetricType[]>) => (this.metricTypes = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMetricTypes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMetricType): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMetricTypes(): void {
    this.eventSubscriber = this.eventManager.subscribe('metricTypeListModification', () => this.loadAll());
  }

  delete(metricType: IMetricType): void {
    const modalRef = this.modalService.open(MetricTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.metricType = metricType;
  }
}
