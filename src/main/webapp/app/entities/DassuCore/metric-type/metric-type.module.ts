import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { MetricTypeComponent } from './metric-type.component';
import { MetricTypeDetailComponent } from './metric-type-detail.component';
import { MetricTypeUpdateComponent } from './metric-type-update.component';
import { MetricTypeDeleteDialogComponent } from './metric-type-delete-dialog.component';
import { metricTypeRoute } from './metric-type.route';

@NgModule({
  imports: [DassuGatewaySharedModule, RouterModule.forChild(metricTypeRoute)],
  declarations: [MetricTypeComponent, MetricTypeDetailComponent, MetricTypeUpdateComponent, MetricTypeDeleteDialogComponent],
  entryComponents: [MetricTypeDeleteDialogComponent],
})
export class DassuCoreMetricTypeModule {}
