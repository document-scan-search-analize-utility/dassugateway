import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMetric } from 'app/shared/model/DassuCore/metric.model';
import { MetricService } from './metric.service';

@Component({
  templateUrl: './metric-delete-dialog.component.html',
})
export class MetricDeleteDialogComponent {
  metric?: IMetric;

  constructor(protected metricService: MetricService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.metricService.delete(id).subscribe(() => {
      this.eventManager.broadcast('metricListModification');
      this.activeModal.close();
    });
  }
}
