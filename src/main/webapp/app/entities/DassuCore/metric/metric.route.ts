import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMetric, Metric } from 'app/shared/model/DassuCore/metric.model';
import { MetricService } from './metric.service';
import { MetricComponent } from './metric.component';
import { MetricDetailComponent } from './metric-detail.component';
import { MetricUpdateComponent } from './metric-update.component';

@Injectable({ providedIn: 'root' })
export class MetricResolve implements Resolve<IMetric> {
  constructor(private service: MetricService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMetric> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((metric: HttpResponse<Metric>) => {
          if (metric.body) {
            return of(metric.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Metric());
  }
}

export const metricRoute: Routes = [
  {
    path: '',
    component: MetricComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetric.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MetricDetailComponent,
    resolve: {
      metric: MetricResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetric.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MetricUpdateComponent,
    resolve: {
      metric: MetricResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetric.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MetricUpdateComponent,
    resolve: {
      metric: MetricResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetric.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
