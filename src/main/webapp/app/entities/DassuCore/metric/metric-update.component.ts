import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMetric, Metric } from 'app/shared/model/DassuCore/metric.model';
import { MetricService } from './metric.service';
import { IMetricType } from 'app/shared/model/DassuCore/metric-type.model';
import { MetricTypeService } from 'app/entities/DassuCore/metric-type/metric-type.service';

@Component({
  selector: 'jhi-metric-update',
  templateUrl: './metric-update.component.html',
})
export class MetricUpdateComponent implements OnInit {
  isSaving = false;
  metrictypes: IMetricType[] = [];

  editForm = this.fb.group({
    id: [],
    key: [],
    value: [],
    metricType: [],
  });

  constructor(
    protected metricService: MetricService,
    protected metricTypeService: MetricTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ metric }) => {
      this.updateForm(metric);

      this.metricTypeService.query().subscribe((res: HttpResponse<IMetricType[]>) => (this.metrictypes = res.body || []));
    });
  }

  updateForm(metric: IMetric): void {
    this.editForm.patchValue({
      id: metric.id,
      key: metric.key,
      value: metric.value,
      metricType: metric.metricType,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const metric = this.createFromForm();
    if (metric.id !== undefined) {
      this.subscribeToSaveResponse(this.metricService.update(metric));
    } else {
      this.subscribeToSaveResponse(this.metricService.create(metric));
    }
  }

  private createFromForm(): IMetric {
    return {
      ...new Metric(),
      id: this.editForm.get(['id'])!.value,
      key: this.editForm.get(['key'])!.value,
      value: this.editForm.get(['value'])!.value,
      metricType: this.editForm.get(['metricType'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMetric>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IMetricType): any {
    return item.id;
  }
}
