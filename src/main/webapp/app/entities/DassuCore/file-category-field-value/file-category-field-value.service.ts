import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';

type EntityResponseType = HttpResponse<IFileCategoryFieldValue>;
type EntityArrayResponseType = HttpResponse<IFileCategoryFieldValue[]>;

@Injectable({ providedIn: 'root' })
export class FileCategoryFieldValueService {
  public resourceUrl = SERVER_API_URL + 'services/dassucore/api/file-category-field-values';

  constructor(protected http: HttpClient) {}

  create(fileCategoryFieldValue: IFileCategoryFieldValue): Observable<EntityResponseType> {
    return this.http.post<IFileCategoryFieldValue>(this.resourceUrl, fileCategoryFieldValue, { observe: 'response' });
  }

  update(fileCategoryFieldValue: IFileCategoryFieldValue): Observable<EntityResponseType> {
    return this.http.put<IFileCategoryFieldValue>(this.resourceUrl, fileCategoryFieldValue, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFileCategoryFieldValue>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFileCategoryFieldValue[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
