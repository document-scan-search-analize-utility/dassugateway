import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';
import { FileCategoryFieldValueService } from './file-category-field-value.service';
import { FileCategoryFieldValueDeleteDialogComponent } from './file-category-field-value-delete-dialog.component';

@Component({
  selector: 'jhi-file-category-field-value',
  templateUrl: './file-category-field-value.component.html',
})
export class FileCategoryFieldValueComponent implements OnInit, OnDestroy {
  fileCategoryFieldValues?: IFileCategoryFieldValue[];
  eventSubscriber?: Subscription;

  constructor(
    protected fileCategoryFieldValueService: FileCategoryFieldValueService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fileCategoryFieldValueService
      .query()
      .subscribe((res: HttpResponse<IFileCategoryFieldValue[]>) => (this.fileCategoryFieldValues = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFileCategoryFieldValues();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFileCategoryFieldValue): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFileCategoryFieldValues(): void {
    this.eventSubscriber = this.eventManager.subscribe('fileCategoryFieldValueListModification', () => this.loadAll());
  }

  delete(fileCategoryFieldValue: IFileCategoryFieldValue): void {
    const modalRef = this.modalService.open(FileCategoryFieldValueDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fileCategoryFieldValue = fileCategoryFieldValue;
  }
}
