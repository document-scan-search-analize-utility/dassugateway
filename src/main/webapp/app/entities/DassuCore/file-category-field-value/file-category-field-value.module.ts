import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { FileCategoryFieldValueComponent } from './file-category-field-value.component';
import { FileCategoryFieldValueDetailComponent } from './file-category-field-value-detail.component';
import { FileCategoryFieldValueUpdateComponent } from './file-category-field-value-update.component';
import { FileCategoryFieldValueDeleteDialogComponent } from './file-category-field-value-delete-dialog.component';
import { fileCategoryFieldValueRoute } from './file-category-field-value.route';

@NgModule({
  imports: [DassuGatewaySharedModule, RouterModule.forChild(fileCategoryFieldValueRoute)],
  declarations: [
    FileCategoryFieldValueComponent,
    FileCategoryFieldValueDetailComponent,
    FileCategoryFieldValueUpdateComponent,
    FileCategoryFieldValueDeleteDialogComponent,
  ],
  entryComponents: [FileCategoryFieldValueDeleteDialogComponent],
})
export class DassuCoreFileCategoryFieldValueModule {}
