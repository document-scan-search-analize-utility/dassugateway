import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IFileCategoryFieldValue, FileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';
import { FileCategoryFieldValueService } from './file-category-field-value.service';
import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { CompositFileService } from 'app/entities/DassuCore/composit-file/composit-file.service';
import { IFileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';
import { FileCategoryFieldService } from 'app/entities/DassuCore/file-category-field/file-category-field.service';
import { IMetric } from 'app/shared/model/DassuCore/metric.model';
import { MetricService } from 'app/entities/DassuCore/metric/metric.service';

type SelectableEntity = ICompositFile | IFileCategoryField | IMetric;

@Component({
  selector: 'jhi-file-category-field-value-update',
  templateUrl: './file-category-field-value-update.component.html',
})
export class FileCategoryFieldValueUpdateComponent implements OnInit {
  isSaving = false;
  compositfiles: ICompositFile[] = [];
  filecategoryfields: IFileCategoryField[] = [];
  metrics: IMetric[] = [];

  editForm = this.fb.group({
    id: [],
    value: [],
    compositFile: [],
    fileCategoryField: [],
    metric: [],
  });

  constructor(
    protected fileCategoryFieldValueService: FileCategoryFieldValueService,
    protected compositFileService: CompositFileService,
    protected fileCategoryFieldService: FileCategoryFieldService,
    protected metricService: MetricService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fileCategoryFieldValue }) => {
      this.updateForm(fileCategoryFieldValue);

      this.compositFileService
        .query({ filter: 'filecategoryfieldvalue-is-null' })
        .pipe(
          map((res: HttpResponse<ICompositFile[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ICompositFile[]) => {
          if (!fileCategoryFieldValue.compositFile || !fileCategoryFieldValue.compositFile.id) {
            this.compositfiles = resBody;
          } else {
            this.compositFileService
              .find(fileCategoryFieldValue.compositFile.id)
              .pipe(
                map((subRes: HttpResponse<ICompositFile>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ICompositFile[]) => (this.compositfiles = concatRes));
          }
        });

      this.fileCategoryFieldService
        .query({ filter: 'filecategoryfieldvalue-is-null' })
        .pipe(
          map((res: HttpResponse<IFileCategoryField[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFileCategoryField[]) => {
          if (!fileCategoryFieldValue.fileCategoryField || !fileCategoryFieldValue.fileCategoryField.id) {
            this.filecategoryfields = resBody;
          } else {
            this.fileCategoryFieldService
              .find(fileCategoryFieldValue.fileCategoryField.id)
              .pipe(
                map((subRes: HttpResponse<IFileCategoryField>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFileCategoryField[]) => (this.filecategoryfields = concatRes));
          }
        });

      this.metricService
        .query({ filter: 'filecategoryfieldvalue-is-null' })
        .pipe(
          map((res: HttpResponse<IMetric[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IMetric[]) => {
          if (!fileCategoryFieldValue.metric || !fileCategoryFieldValue.metric.id) {
            this.metrics = resBody;
          } else {
            this.metricService
              .find(fileCategoryFieldValue.metric.id)
              .pipe(
                map((subRes: HttpResponse<IMetric>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IMetric[]) => (this.metrics = concatRes));
          }
        });
    });
  }

  updateForm(fileCategoryFieldValue: IFileCategoryFieldValue): void {
    this.editForm.patchValue({
      id: fileCategoryFieldValue.id,
      value: fileCategoryFieldValue.value,
      compositFile: fileCategoryFieldValue.compositFile,
      fileCategoryField: fileCategoryFieldValue.fileCategoryField,
      metric: fileCategoryFieldValue.metric,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fileCategoryFieldValue = this.createFromForm();
    if (fileCategoryFieldValue.id !== undefined) {
      this.subscribeToSaveResponse(this.fileCategoryFieldValueService.update(fileCategoryFieldValue));
    } else {
      this.subscribeToSaveResponse(this.fileCategoryFieldValueService.create(fileCategoryFieldValue));
    }
  }

  private createFromForm(): IFileCategoryFieldValue {
    return {
      ...new FileCategoryFieldValue(),
      id: this.editForm.get(['id'])!.value,
      value: this.editForm.get(['value'])!.value,
      compositFile: this.editForm.get(['compositFile'])!.value,
      fileCategoryField: this.editForm.get(['fileCategoryField'])!.value,
      metric: this.editForm.get(['metric'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFileCategoryFieldValue>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
