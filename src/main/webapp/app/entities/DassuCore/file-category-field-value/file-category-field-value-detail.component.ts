import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';

@Component({
  selector: 'jhi-file-category-field-value-detail',
  templateUrl: './file-category-field-value-detail.component.html',
})
export class FileCategoryFieldValueDetailComponent implements OnInit {
  fileCategoryFieldValue: IFileCategoryFieldValue | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fileCategoryFieldValue }) => (this.fileCategoryFieldValue = fileCategoryFieldValue));
  }

  previousState(): void {
    window.history.back();
  }
}
