import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';
import { FileCategoryFieldValueService } from './file-category-field-value.service';

@Component({
  templateUrl: './file-category-field-value-delete-dialog.component.html',
})
export class FileCategoryFieldValueDeleteDialogComponent {
  fileCategoryFieldValue?: IFileCategoryFieldValue;

  constructor(
    protected fileCategoryFieldValueService: FileCategoryFieldValueService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fileCategoryFieldValueService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fileCategoryFieldValueListModification');
      this.activeModal.close();
    });
  }
}
