import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFileCategoryFieldValue, FileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';
import { FileCategoryFieldValueService } from './file-category-field-value.service';
import { FileCategoryFieldValueComponent } from './file-category-field-value.component';
import { FileCategoryFieldValueDetailComponent } from './file-category-field-value-detail.component';
import { FileCategoryFieldValueUpdateComponent } from './file-category-field-value-update.component';

@Injectable({ providedIn: 'root' })
export class FileCategoryFieldValueResolve implements Resolve<IFileCategoryFieldValue> {
  constructor(private service: FileCategoryFieldValueService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFileCategoryFieldValue> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fileCategoryFieldValue: HttpResponse<FileCategoryFieldValue>) => {
          if (fileCategoryFieldValue.body) {
            return of(fileCategoryFieldValue.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FileCategoryFieldValue());
  }
}

export const fileCategoryFieldValueRoute: Routes = [
  {
    path: '',
    component: FileCategoryFieldValueComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryFieldValue.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FileCategoryFieldValueDetailComponent,
    resolve: {
      fileCategoryFieldValue: FileCategoryFieldValueResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryFieldValue.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FileCategoryFieldValueUpdateComponent,
    resolve: {
      fileCategoryFieldValue: FileCategoryFieldValueResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryFieldValue.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FileCategoryFieldValueUpdateComponent,
    resolve: {
      fileCategoryFieldValue: FileCategoryFieldValueResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryFieldValue.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
