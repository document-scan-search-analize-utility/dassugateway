import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { BinaryFileService } from './binary-file.service';

@Component({
  templateUrl: './binary-file-delete-dialog.component.html',
})
export class BinaryFileDeleteDialogComponent {
  binaryFile?: IBinaryFile;

  constructor(
    protected binaryFileService: BinaryFileService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.binaryFileService.delete(id).subscribe(() => {
      this.eventManager.broadcast('binaryFileListModification');
      this.activeModal.close();
    });
  }
}
