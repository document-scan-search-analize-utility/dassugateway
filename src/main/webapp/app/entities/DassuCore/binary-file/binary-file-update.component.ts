import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBinaryFile, BinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { BinaryFileService } from './binary-file.service';

@Component({
  selector: 'jhi-binary-file-update',
  templateUrl: './binary-file-update.component.html',
})
export class BinaryFileUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    version: [],
    storageFilePath: [],
    md5: [],
    storageType: [],
  });

  constructor(protected binaryFileService: BinaryFileService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ binaryFile }) => {
      this.updateForm(binaryFile);
    });
  }

  updateForm(binaryFile: IBinaryFile): void {
    this.editForm.patchValue({
      id: binaryFile.id,
      name: binaryFile.name,
      version: binaryFile.version,
      storageFilePath: binaryFile.storageFilePath,
      md5: binaryFile.md5,
      storageType: binaryFile.storageType,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const binaryFile = this.createFromForm();
    if (binaryFile.id !== undefined) {
      this.subscribeToSaveResponse(this.binaryFileService.update(binaryFile));
    } else {
      this.subscribeToSaveResponse(this.binaryFileService.create(binaryFile));
    }
  }

  private createFromForm(): IBinaryFile {
    return {
      ...new BinaryFile(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      version: this.editForm.get(['version'])!.value,
      storageFilePath: this.editForm.get(['storageFilePath'])!.value,
      md5: this.editForm.get(['md5'])!.value,
      storageType: this.editForm.get(['storageType'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBinaryFile>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
