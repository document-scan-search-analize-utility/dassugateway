import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { BinaryFileService } from './binary-file.service';
import { BinaryFileDeleteDialogComponent } from './binary-file-delete-dialog.component';

@Component({
  selector: 'jhi-binary-file',
  templateUrl: './binary-file.component.html',
})
export class BinaryFileComponent implements OnInit, OnDestroy {
  binaryFiles?: IBinaryFile[];
  eventSubscriber?: Subscription;

  constructor(protected binaryFileService: BinaryFileService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.binaryFileService.query().subscribe((res: HttpResponse<IBinaryFile[]>) => (this.binaryFiles = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBinaryFiles();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBinaryFile): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBinaryFiles(): void {
    this.eventSubscriber = this.eventManager.subscribe('binaryFileListModification', () => this.loadAll());
  }

  delete(binaryFile: IBinaryFile): void {
    const modalRef = this.modalService.open(BinaryFileDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.binaryFile = binaryFile;
  }
}
