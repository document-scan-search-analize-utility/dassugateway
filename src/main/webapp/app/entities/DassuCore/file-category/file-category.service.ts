import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';

type EntityResponseType = HttpResponse<IFileCategory>;
type EntityArrayResponseType = HttpResponse<IFileCategory[]>;

@Injectable({ providedIn: 'root' })
export class FileCategoryService {
  public resourceUrl = SERVER_API_URL + 'services/dassucore/api/file-categories';

  constructor(protected http: HttpClient) {}

  create(fileCategory: IFileCategory): Observable<EntityResponseType> {
    return this.http.post<IFileCategory>(this.resourceUrl, fileCategory, { observe: 'response' });
  }

  update(fileCategory: IFileCategory): Observable<EntityResponseType> {
    return this.http.put<IFileCategory>(this.resourceUrl, fileCategory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFileCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFileCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
