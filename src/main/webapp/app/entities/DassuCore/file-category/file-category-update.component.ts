import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IFileCategory, FileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { FileCategoryService } from './file-category.service';
import { IMetric } from 'app/shared/model/DassuCore/metric.model';
import { MetricService } from 'app/entities/DassuCore/metric/metric.service';

@Component({
  selector: 'jhi-file-category-update',
  templateUrl: './file-category-update.component.html',
})
export class FileCategoryUpdateComponent implements OnInit {
  isSaving = false;
  metrics: IMetric[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    description: [],
    metric: [],
  });

  constructor(
    protected fileCategoryService: FileCategoryService,
    protected metricService: MetricService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fileCategory }) => {
      this.updateForm(fileCategory);

      this.metricService
        .query({ filter: 'filecategory-is-null' })
        .pipe(
          map((res: HttpResponse<IMetric[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IMetric[]) => {
          if (!fileCategory.metric || !fileCategory.metric.id) {
            this.metrics = resBody;
          } else {
            this.metricService
              .find(fileCategory.metric.id)
              .pipe(
                map((subRes: HttpResponse<IMetric>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IMetric[]) => (this.metrics = concatRes));
          }
        });
    });
  }

  updateForm(fileCategory: IFileCategory): void {
    this.editForm.patchValue({
      id: fileCategory.id,
      name: fileCategory.name,
      description: fileCategory.description,
      metric: fileCategory.metric,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fileCategory = this.createFromForm();
    if (fileCategory.id !== undefined) {
      this.subscribeToSaveResponse(this.fileCategoryService.update(fileCategory));
    } else {
      this.subscribeToSaveResponse(this.fileCategoryService.create(fileCategory));
    }
  }

  private createFromForm(): IFileCategory {
    return {
      ...new FileCategory(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      metric: this.editForm.get(['metric'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFileCategory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IMetric): any {
    return item.id;
  }
}
