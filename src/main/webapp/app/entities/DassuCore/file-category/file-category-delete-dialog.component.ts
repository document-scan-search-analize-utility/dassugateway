import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { FileCategoryService } from './file-category.service';

@Component({
  templateUrl: './file-category-delete-dialog.component.html',
})
export class FileCategoryDeleteDialogComponent {
  fileCategory?: IFileCategory;

  constructor(
    protected fileCategoryService: FileCategoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fileCategoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fileCategoryListModification');
      this.activeModal.close();
    });
  }
}
