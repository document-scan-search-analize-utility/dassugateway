import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICompositFile, CompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { CompositFileService } from './composit-file.service';
import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { BinaryFileService } from 'app/entities/DassuCore/binary-file/binary-file.service';
import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { FileCategoryService } from 'app/entities/DassuCore/file-category/file-category.service';

type SelectableEntity = IBinaryFile | IFileCategory;

@Component({
  selector: 'jhi-composit-file-update',
  templateUrl: './composit-file-update.component.html',
})
export class CompositFileUpdateComponent implements OnInit {
  isSaving = false;
  binaryfiles: IBinaryFile[] = [];
  filecategories: IFileCategory[] = [];

  editForm = this.fb.group({
    id: [],
    status: [],
    binaryFile: [],
    fileCategory: [],
  });

  constructor(
    protected compositFileService: CompositFileService,
    protected binaryFileService: BinaryFileService,
    protected fileCategoryService: FileCategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ compositFile }) => {
      this.updateForm(compositFile);

      this.binaryFileService
        .query({ filter: 'composit-is-null' })
        .pipe(
          map((res: HttpResponse<IBinaryFile[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBinaryFile[]) => {
          if (!compositFile.binaryFile || !compositFile.binaryFile.id) {
            this.binaryfiles = resBody;
          } else {
            this.binaryFileService
              .find(compositFile.binaryFile.id)
              .pipe(
                map((subRes: HttpResponse<IBinaryFile>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBinaryFile[]) => (this.binaryfiles = concatRes));
          }
        });

      this.fileCategoryService.query().subscribe((res: HttpResponse<IFileCategory[]>) => (this.filecategories = res.body || []));
    });
  }

  updateForm(compositFile: ICompositFile): void {
    this.editForm.patchValue({
      id: compositFile.id,
      status: compositFile.status,
      binaryFile: compositFile.binaryFile,
      fileCategory: compositFile.fileCategory,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const compositFile = this.createFromForm();
    if (compositFile.id !== undefined) {
      this.subscribeToSaveResponse(this.compositFileService.update(compositFile));
    } else {
      this.subscribeToSaveResponse(this.compositFileService.create(compositFile));
    }
  }

  private createFromForm(): ICompositFile {
    return {
      ...new CompositFile(),
      id: this.editForm.get(['id'])!.value,
      status: this.editForm.get(['status'])!.value,
      binaryFile: this.editForm.get(['binaryFile'])!.value,
      fileCategory: this.editForm.get(['fileCategory'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompositFile>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
