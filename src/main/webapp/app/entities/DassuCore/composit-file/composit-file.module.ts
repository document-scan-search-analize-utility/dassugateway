import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { CompositFileComponent } from './composit-file.component';
import { CompositFileDetailComponent } from './composit-file-detail.component';
import { CompositFileUpdateComponent } from './composit-file-update.component';
import { CompositFileDeleteDialogComponent } from './composit-file-delete-dialog.component';
import { compositFileRoute } from './composit-file.route';

@NgModule({
  imports: [DassuGatewaySharedModule, RouterModule.forChild(compositFileRoute)],
  declarations: [CompositFileComponent, CompositFileDetailComponent, CompositFileUpdateComponent, CompositFileDeleteDialogComponent],
  entryComponents: [CompositFileDeleteDialogComponent],
})
export class DassuCoreCompositFileModule {}
