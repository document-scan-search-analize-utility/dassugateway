import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { CompositFileService } from './composit-file.service';
import { CompositFileDeleteDialogComponent } from './composit-file-delete-dialog.component';

@Component({
  selector: 'jhi-composit-file',
  templateUrl: './composit-file.component.html',
})
export class CompositFileComponent implements OnInit, OnDestroy {
  compositFiles?: ICompositFile[];
  eventSubscriber?: Subscription;

  constructor(
    protected compositFileService: CompositFileService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.compositFileService.query().subscribe((res: HttpResponse<ICompositFile[]>) => (this.compositFiles = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCompositFiles();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICompositFile): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCompositFiles(): void {
    this.eventSubscriber = this.eventManager.subscribe('compositFileListModification', () => this.loadAll());
  }

  delete(compositFile: ICompositFile): void {
    const modalRef = this.modalService.open(CompositFileDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.compositFile = compositFile;
  }
}
