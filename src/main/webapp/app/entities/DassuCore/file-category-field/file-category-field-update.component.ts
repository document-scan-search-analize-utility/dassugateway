import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFileCategoryField, FileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';
import { FileCategoryFieldService } from './file-category-field.service';
import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { FileCategoryService } from 'app/entities/DassuCore/file-category/file-category.service';

@Component({
  selector: 'jhi-file-category-field-update',
  templateUrl: './file-category-field-update.component.html',
})
export class FileCategoryFieldUpdateComponent implements OnInit {
  isSaving = false;
  filecategories: IFileCategory[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    meta: [],
    fileCategories: [],
  });

  constructor(
    protected fileCategoryFieldService: FileCategoryFieldService,
    protected fileCategoryService: FileCategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fileCategoryField }) => {
      this.updateForm(fileCategoryField);

      this.fileCategoryService.query().subscribe((res: HttpResponse<IFileCategory[]>) => (this.filecategories = res.body || []));
    });
  }

  updateForm(fileCategoryField: IFileCategoryField): void {
    this.editForm.patchValue({
      id: fileCategoryField.id,
      name: fileCategoryField.name,
      meta: fileCategoryField.meta,
      fileCategories: fileCategoryField.fileCategories,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fileCategoryField = this.createFromForm();
    if (fileCategoryField.id !== undefined) {
      this.subscribeToSaveResponse(this.fileCategoryFieldService.update(fileCategoryField));
    } else {
      this.subscribeToSaveResponse(this.fileCategoryFieldService.create(fileCategoryField));
    }
  }

  private createFromForm(): IFileCategoryField {
    return {
      ...new FileCategoryField(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      meta: this.editForm.get(['meta'])!.value,
      fileCategories: this.editForm.get(['fileCategories'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFileCategoryField>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IFileCategory): any {
    return item.id;
  }

  getSelected(selectedVals: IFileCategory[], option: IFileCategory): IFileCategory {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
