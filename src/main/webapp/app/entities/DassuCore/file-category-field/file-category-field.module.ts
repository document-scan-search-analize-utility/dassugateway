import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { FileCategoryFieldComponent } from './file-category-field.component';
import { FileCategoryFieldDetailComponent } from './file-category-field-detail.component';
import { FileCategoryFieldUpdateComponent } from './file-category-field-update.component';
import { FileCategoryFieldDeleteDialogComponent } from './file-category-field-delete-dialog.component';
import { fileCategoryFieldRoute } from './file-category-field.route';

@NgModule({
  imports: [DassuGatewaySharedModule, RouterModule.forChild(fileCategoryFieldRoute)],
  declarations: [
    FileCategoryFieldComponent,
    FileCategoryFieldDetailComponent,
    FileCategoryFieldUpdateComponent,
    FileCategoryFieldDeleteDialogComponent,
  ],
  entryComponents: [FileCategoryFieldDeleteDialogComponent],
})
export class DassuCoreFileCategoryFieldModule {}
