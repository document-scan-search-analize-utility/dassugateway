import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';
import { FileCategoryFieldService } from './file-category-field.service';

@Component({
  templateUrl: './file-category-field-delete-dialog.component.html',
})
export class FileCategoryFieldDeleteDialogComponent {
  fileCategoryField?: IFileCategoryField;

  constructor(
    protected fileCategoryFieldService: FileCategoryFieldService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fileCategoryFieldService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fileCategoryFieldListModification');
      this.activeModal.close();
    });
  }
}
