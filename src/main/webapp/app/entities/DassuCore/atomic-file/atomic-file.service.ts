import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';

type EntityResponseType = HttpResponse<IAtomicFile>;
type EntityArrayResponseType = HttpResponse<IAtomicFile[]>;

@Injectable({ providedIn: 'root' })
export class AtomicFileService {
  public resourceUrl = SERVER_API_URL + 'services/dassucore/api/atomic-files';

  constructor(protected http: HttpClient) {}

  create(atomicFile: IAtomicFile): Observable<EntityResponseType> {
    return this.http.post<IAtomicFile>(this.resourceUrl, atomicFile, { observe: 'response' });
  }

  update(atomicFile: IAtomicFile): Observable<EntityResponseType> {
    return this.http.put<IAtomicFile>(this.resourceUrl, atomicFile, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAtomicFile>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAtomicFile[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
