import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';

@Component({
  selector: 'jhi-atomic-file-detail',
  templateUrl: './atomic-file-detail.component.html',
})
export class AtomicFileDetailComponent implements OnInit {
  atomicFile: IAtomicFile | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ atomicFile }) => (this.atomicFile = atomicFile));
  }

  previousState(): void {
    window.history.back();
  }
}
