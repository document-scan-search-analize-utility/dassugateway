import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';
import { AtomicFileService } from './atomic-file.service';

@Component({
  templateUrl: './atomic-file-delete-dialog.component.html',
})
export class AtomicFileDeleteDialogComponent {
  atomicFile?: IAtomicFile;

  constructor(
    protected atomicFileService: AtomicFileService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.atomicFileService.delete(id).subscribe(() => {
      this.eventManager.broadcast('atomicFileListModification');
      this.activeModal.close();
    });
  }
}
