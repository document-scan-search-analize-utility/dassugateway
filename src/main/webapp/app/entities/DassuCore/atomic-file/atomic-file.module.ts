import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { AtomicFileComponent } from './atomic-file.component';
import { AtomicFileDetailComponent } from './atomic-file-detail.component';
import { AtomicFileUpdateComponent } from './atomic-file-update.component';
import { AtomicFileDeleteDialogComponent } from './atomic-file-delete-dialog.component';
import { atomicFileRoute } from './atomic-file.route';

@NgModule({
  imports: [DassuGatewaySharedModule, RouterModule.forChild(atomicFileRoute)],
  declarations: [AtomicFileComponent, AtomicFileDetailComponent, AtomicFileUpdateComponent, AtomicFileDeleteDialogComponent],
  entryComponents: [AtomicFileDeleteDialogComponent],
})
export class DassuCoreAtomicFileModule {}
