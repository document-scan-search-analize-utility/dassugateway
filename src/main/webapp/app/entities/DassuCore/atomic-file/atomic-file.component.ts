import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';
import { AtomicFileService } from './atomic-file.service';
import { AtomicFileDeleteDialogComponent } from './atomic-file-delete-dialog.component';

@Component({
  selector: 'jhi-atomic-file',
  templateUrl: './atomic-file.component.html',
})
export class AtomicFileComponent implements OnInit, OnDestroy {
  atomicFiles?: IAtomicFile[];
  eventSubscriber?: Subscription;

  constructor(protected atomicFileService: AtomicFileService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.atomicFileService.query().subscribe((res: HttpResponse<IAtomicFile[]>) => (this.atomicFiles = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAtomicFiles();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAtomicFile): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAtomicFiles(): void {
    this.eventSubscriber = this.eventManager.subscribe('atomicFileListModification', () => this.loadAll());
  }

  delete(atomicFile: IAtomicFile): void {
    const modalRef = this.modalService.open(AtomicFileDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.atomicFile = atomicFile;
  }
}
