import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAtomicFile, AtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';
import { AtomicFileService } from './atomic-file.service';
import { AtomicFileComponent } from './atomic-file.component';
import { AtomicFileDetailComponent } from './atomic-file-detail.component';
import { AtomicFileUpdateComponent } from './atomic-file-update.component';

@Injectable({ providedIn: 'root' })
export class AtomicFileResolve implements Resolve<IAtomicFile> {
  constructor(private service: AtomicFileService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAtomicFile> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((atomicFile: HttpResponse<AtomicFile>) => {
          if (atomicFile.body) {
            return of(atomicFile.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AtomicFile());
  }
}

export const atomicFileRoute: Routes = [
  {
    path: '',
    component: AtomicFileComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreAtomicFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AtomicFileDetailComponent,
    resolve: {
      atomicFile: AtomicFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreAtomicFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AtomicFileUpdateComponent,
    resolve: {
      atomicFile: AtomicFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreAtomicFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AtomicFileUpdateComponent,
    resolve: {
      atomicFile: AtomicFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreAtomicFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
