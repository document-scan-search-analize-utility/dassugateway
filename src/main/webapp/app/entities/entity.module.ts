import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'binary-file',
        loadChildren: () => import('./DassuCore/binary-file/binary-file.module').then(m => m.DassuCoreBinaryFileModule),
      },
      {
        path: 'composit-file',
        loadChildren: () => import('./DassuCore/composit-file/composit-file.module').then(m => m.DassuCoreCompositFileModule),
      },
      {
        path: 'atomic-file',
        loadChildren: () => import('./DassuCore/atomic-file/atomic-file.module').then(m => m.DassuCoreAtomicFileModule),
      },
      {
        path: 'metric',
        loadChildren: () => import('./DassuCore/metric/metric.module').then(m => m.DassuCoreMetricModule),
      },
      {
        path: 'metric-type',
        loadChildren: () => import('./DassuCore/metric-type/metric-type.module').then(m => m.DassuCoreMetricTypeModule),
      },
      {
        path: 'file-category',
        loadChildren: () => import('./DassuCore/file-category/file-category.module').then(m => m.DassuCoreFileCategoryModule),
      },
      {
        path: 'file-category-field',
        loadChildren: () =>
          import('./DassuCore/file-category-field/file-category-field.module').then(m => m.DassuCoreFileCategoryFieldModule),
      },
      {
        path: 'file-category-field-value',
        loadChildren: () =>
          import('./DassuCore/file-category-field-value/file-category-field-value.module').then(
            m => m.DassuCoreFileCategoryFieldValueModule
          ),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class DassuGatewayEntityModule {}
