import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnnotatePageRoutingModule } from './annotate-page-routing.module';
import { AnnotatePageComponent } from './annotate-page.component';
import { DassuGatewaySharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [AnnotatePageComponent],
  imports: [CommonModule, AnnotatePageRoutingModule, DassuGatewaySharedModule],
})
export class AnnotatePageModule {}
