export const enum ProcessingStatus {
  CREATED = 'CREATED',

  PROCESSING = 'PROCESSING',

  PARSED = 'PARSED',
}
