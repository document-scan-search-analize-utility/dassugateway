import { IMetric } from 'app/shared/model/DassuCore/metric.model';
import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { IFileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';

export interface IFileCategory {
  id?: number;
  name?: string;
  description?: string;
  metric?: IMetric;
  composits?: ICompositFile[];
  fileCategoryFields?: IFileCategoryField[];
}

export class FileCategory implements IFileCategory {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public metric?: IMetric,
    public composits?: ICompositFile[],
    public fileCategoryFields?: IFileCategoryField[]
  ) {}
}
