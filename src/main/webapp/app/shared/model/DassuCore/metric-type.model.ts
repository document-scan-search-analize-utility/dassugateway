import { IMetric } from 'app/shared/model/DassuCore/metric.model';

export interface IMetricType {
  id?: number;
  name?: string;
  metrics?: IMetric[];
}

export class MetricType implements IMetricType {
  constructor(public id?: number, public name?: string, public metrics?: IMetric[]) {}
}
