import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';

export interface IAtomicFile {
  id?: number;
  annotatedData?: string;
  annotatedText?: string;
  binaryFile?: IBinaryFile;
  parent?: ICompositFile;
}

export class AtomicFile implements IAtomicFile {
  constructor(
    public id?: number,
    public annotatedData?: string,
    public annotatedText?: string,
    public binaryFile?: IBinaryFile,
    public parent?: ICompositFile
  ) {}
}
