import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { IFileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';
import { IMetric } from 'app/shared/model/DassuCore/metric.model';

export interface IFileCategoryFieldValue {
  id?: number;
  value?: string;
  compositFile?: ICompositFile;
  fileCategoryField?: IFileCategoryField;
  metric?: IMetric;
}

export class FileCategoryFieldValue implements IFileCategoryFieldValue {
  constructor(
    public id?: number,
    public value?: string,
    public compositFile?: ICompositFile,
    public fileCategoryField?: IFileCategoryField,
    public metric?: IMetric
  ) {}
}
