import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { IFileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';

export interface IFileCategoryField {
  id?: number;
  name?: string;
  meta?: string;
  fileCategories?: IFileCategory[];
  fileCategoryFieldValue?: IFileCategoryFieldValue;
}

export class FileCategoryField implements IFileCategoryField {
  constructor(
    public id?: number,
    public name?: string,
    public meta?: string,
    public fileCategories?: IFileCategory[],
    public fileCategoryFieldValue?: IFileCategoryFieldValue
  ) {}
}
