import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { IAtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';
import { IFileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';
import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { ProcessingStatus } from 'app/shared/model/enumerations/processing-status.model';

export interface ICompositFile {
  id?: number;
  status?: ProcessingStatus;
  binaryFile?: IBinaryFile;
  atomics?: IAtomicFile[];
  fileCategoryFieldValue?: IFileCategoryFieldValue;
  fileCategory?: IFileCategory;
}

export class CompositFile implements ICompositFile {
  constructor(
    public id?: number,
    public status?: ProcessingStatus,
    public binaryFile?: IBinaryFile,
    public atomics?: IAtomicFile[],
    public fileCategoryFieldValue?: IFileCategoryFieldValue,
    public fileCategory?: IFileCategory
  ) {}
}
