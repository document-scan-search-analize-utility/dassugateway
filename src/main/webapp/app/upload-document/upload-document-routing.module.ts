import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Authority } from 'app/shared/constants/authority.constants';
import { UploadDocumentComponent } from 'app/upload-document/upload-document.component';

const routes: Routes = [
  {
    path: '',
    component: UploadDocumentComponent,
    data: {
      authorities: [Authority.USER, Authority.ADMIN],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadDocumentRoutingModule {}
