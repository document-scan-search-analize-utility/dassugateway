import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UploadDocumentRoutingModule } from './upload-document-routing.module';
import { UploadDocumentComponent } from './upload-document.component';
import { DassuGatewaySharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [UploadDocumentComponent],
  imports: [CommonModule, UploadDocumentRoutingModule, DassuGatewaySharedModule],
})
export class UploadDocumentModule {}
