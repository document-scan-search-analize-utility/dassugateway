import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        {
          path: 'annotate-page',
          data: {
            authorities: [Authority.USER, Authority.ADMIN],
          },
          loadChildren: () => import('./annotate-page/annotate-page.module').then(m => m.AnnotatePageModule),
        },
        {
          path: 'upload',
          data: {
            authorities: [Authority.USER, Authority.ADMIN],
          },
          loadChildren: () => import('./upload-document/upload-document.module').then(m => m.UploadDocumentModule),
        },
        ...LAYOUT_ROUTES,
      ],
      { enableTracing: false }
    ),
  ],
  exports: [RouterModule],
})
export class DassuGatewayAppRoutingModule {}

// { enableTracing: DEBUG_INFO_ENABLED }
