import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import './vendor';
import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { DassuGatewayCoreModule } from 'app/core/core.module';
import { DassuGatewayAppRoutingModule } from './app-routing.module';
import { DassuGatewayHomeModule } from './home/home.module';
import { DassuGatewayEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/camelcase */
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { AnnotatePageModule } from 'app/annotate-page/annotate-page.module';
import { UploadDocumentModule } from 'app/upload-document/upload-document.module';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key]);

registerLocaleData(en);

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AnnotatePageModule,
    UploadDocumentModule,
    DassuGatewaySharedModule,
    DassuGatewayCoreModule,
    DassuGatewayHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    DassuGatewayEntityModule,
    DassuGatewayAppRoutingModule,
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: icons },
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class DassuGatewayAppModule {}
