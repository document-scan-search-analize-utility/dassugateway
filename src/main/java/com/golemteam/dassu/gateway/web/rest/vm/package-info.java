/**
 * View Models used by Spring MVC REST controllers.
 */
package com.golemteam.dassu.gateway.web.rest.vm;
